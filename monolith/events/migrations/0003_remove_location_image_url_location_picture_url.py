# Generated by Django 4.0.3 on 2023-03-24 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_image_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='location',
            name='image_url',
        ),
        migrations.AddField(
            model_name='location',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
