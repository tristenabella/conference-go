from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city):
    headers = {"Authorization": PEXELS_API_KEY}
    # photo_params = {
    #     "Authorization": PEXELS_API_KEY
    # }
    picture_url = (f"https://api.pexels.com/v1/search?query={city}",)
    response = requests.get(picture_url, headers=headers)
    # response = requests.get(
    #     url=f"https://api.pexels.com/v1/search?query={city}",
    #     headers={"Authorization": PEXELS_API_KEY},
    # )
    try:
        return response.json()["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_codingurl = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geo_codingurl, params=geo_params)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(weather_url, params=weather_params)
    content = response.json()

    try:
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
    except (KeyError, IndexError):
        return None

    return {"description": description, "temp": temp}
