from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_accountvo_object(ch, method, properties, body):
    print("Received %r" % body)
    content = json.loads(body)

    email = content["email"]
    first_name = content["first_name"]
    last_name = content["last_name"]
    is_active = content["is_active"]
    updated_string = content["updated"]

    updated = datetime.fromisoformat(updated_string)

    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            first_name=first_name,
            last_name=last_name,
            is_active=is_active,
            updated=updated,
        )
    else:
        AccountVO.objects.filter(email=email).delete()


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(queue=queue_name, exchange="account_info")
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_accountvo_object,
            auto_ack=True,
        )

        print(" [*] Waiting for messages. To exit press CTRL+C")
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
